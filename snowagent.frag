#
# The Snow Inventory Agent reports installed software back to a
# central UIS server.  For now, it's only installed on VMs.
#
snowagent                     6.3.0-1
mcs-config-snowagent	      1
